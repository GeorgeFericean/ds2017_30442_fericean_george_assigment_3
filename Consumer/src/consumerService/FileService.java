package consumerService;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import producer.DVD;


public class FileService {

	public static void doWriteFile(DVD message) throws FileNotFoundException, UnsupportedEncodingException {
		String filename = message.getTitle();
		PrintWriter writer = new PrintWriter(filename + ".txt", "UTF-8");
		writer.println("title: " + message.getTitle());
		writer.println("year: " + message.getYear());
		writer.println("price: " + message.getPrice());
		writer.close();
	}
}
