package consumer;

import java.io.IOException;

import com.rabbitmq.client.AMQP.BasicProperties;

import consumerService.FileService;
import consumerService.MailService;

import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import producer.DVD;
public class QueueConsumer extends ConsumerEndPoint implements Runnable, Consumer {

	private String to;
	
	public QueueConsumer(String to) throws IOException {
		super();
		this.to = to;
	}

	public void run() {
		try {
			// start consuming messages. Auto acknowledge messages.
			super.getChannel().basicConsume(super.getQueueName(), true, this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Called when consumer is registered.
	 */
	public void handleConsumeOk(String consumerTag) {
		System.out.println("Consumer " + consumerTag + " registered");
	}

	
	public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body)
			throws IOException {

		DVD dvd = null;
		try {
			dvd = (DVD) DVD.deserialize(body);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(dvd == null)
		{
			System.out.println("Recieved null from queue\n");
			return;
		}
		
		System.out.println("DVD: " + dvd + " received.");
		
		// send mail
		MailService mailService = new MailService("mailforapp67@gmail.com", "IamPassword");
		mailService.sendMail(to, dvd.getTitle() + " notification", "Recieved " + dvd + " from queue\n");
		System.out.println("Mail sent to " + to);
		// write file
		FileService.doWriteFile(dvd);
		System.out.println("File with " + dvd + " written.");

	}

	public void handleCancel(String consumerTag) {
		System.out.println("Consumer " + consumerTag + ": handleCancel() called");
	}

	public void handleCancelOk(String consumerTag) {
		System.out.println("Consumer " + consumerTag + ": handleCancelOk() called");
	}

	public void handleRecoverOk(String consumerTag) {
		System.out.println("Consumer " + consumerTag + ": handleRecoverOk() called");
	}

	public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {
		System.out.println("Consumer " + consumerTag + ": handleShutdownSignal() called");
	}
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

}