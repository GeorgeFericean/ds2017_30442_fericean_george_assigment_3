package consumer;
import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public abstract class ConsumerEndPoint{
	
	private Channel channel;
    private Connection connection;
    private String queueName;
	
    public ConsumerEndPoint() throws IOException{
        
		
         //Create a connection factory
         ConnectionFactory factory = new ConnectionFactory();
	    
         //hostname of your rabbitmq server
         factory.setHost("localhost");
		
         //getting a connection
         connection = factory.newConnection();
	    
         //creating a channel
         channel = connection.createChannel();
	    
         //declaring a queue for this channel. If queue does not exist,
         //it will be created on the server.
         queueName = channel.queueDeclare().getQueue();
         
         channel.queueBind(queueName, "dvds", "");
    }
	
	
    
    public Channel getChannel() {
		return channel;
	}



	public void setChannel(Channel channel) {
		this.channel = channel;
	}



	public Connection getConnection() {
		return connection;
	}



	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	



	public String getQueueName() {
		return queueName;
	}



	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}



	/**
     * Close channel and connection. Not necessary as it happens implicitly any way. 
     * @throws IOException
     */
     public void close() throws IOException{
         this.channel.close();
         this.connection.close();
     }
}