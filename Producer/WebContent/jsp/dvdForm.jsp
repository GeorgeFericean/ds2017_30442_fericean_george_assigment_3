<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DVD Form</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/style.css">
</head>

<body>
	<div align="center">
		<font size="7"  > 
			Enter enter a new DVD:</font>
	</div>
	<div id="dvd">

		<form name='dvdForm' action="<c:url value='/producer' />" method='POST'>
			<label>Title:</label><input type="text" id="dvdTitle" name="dvdTitle" placeholder="Title">
			<label>Year:</label><input type="number" id="dvdYear" name="dvdYear" placeholder="Year">
			<label>Price:</label><input type="number" id="dvdPrice" name="dvdPrice" placeholder="Price">
			<input name="Submit" type="submit" value="Submit" />
		</form>

		<div align="center">
			<font size="6" color="red"><br>${msg}</font>
		</div>

	</div>
</body>
</html>