package producerController;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import producer.DVD;
import producer.ProducerEndPoint;

public class ProducerController extends HttpServlet {

	private static final long serialVersionUID = 842529818199645205L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		RequestDispatcher rd = request.getRequestDispatcher("/jsp/dvdForm.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// get params
		String dvdTitle = request.getParameter("dvdTitle").toString();
		int dvdYear = Integer.parseInt(request.getParameter("dvdYear").toString());
		double dvdPrice = Double.parseDouble(request.getParameter("dvdPrice").toString());

		// create object
		DVD dvd = new DVD(dvdTitle, dvdYear, dvdPrice);

		// initialize producer (connection + channel)
		ProducerEndPoint producer = new ProducerEndPoint();

		// publish the new dvd
		producer.getChannel().basicPublish("dvds", "", null, DVD.serialize(dvd));

		// close channel and connection
		producer.close();

		System.out.println("DVD " + dvd + " sent.");

		try {
			request.getRequestDispatcher("/jsp/dvdForm.jsp").forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
